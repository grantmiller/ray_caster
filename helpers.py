def PointInsideRect(pointX, pointY, x1, y1, x2, y2):
    ret = (x1 - 1) <= pointX <= (x2 + 1) and (y1 - 1) <= pointY <= (y2 + 1)
    return ret
