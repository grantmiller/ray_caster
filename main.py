import random
import time
from tkinter import Tk, Canvas
import canvas2d
import threading

import canvas3d
from game import Game
from units import Player, Wall, NPC

WIDTH = 1280
HEIGHT = 720

root = Tk()
root.title('Ray Casting Game')
canvas = Canvas(root, width=WIDTH, height=HEIGHT, background='black')
canvas.pack()

game = Game(width=WIDTH / 2, height=HEIGHT)

# player
game.AddUnit(Player(
    x=game.width / 2,
    y=game.height / 2,
    game=game,
))
game.AddUnit(NPC(
    x=random.randint(0, game.width),
    y=random.randint(0, game.height),
    game=game,
))
# game.WallsAroundEdge()
game.RandomWalls()

# events #################################
for key in [
    'w', 'a', 's', 'd',
]:
    root.bind('<KeyPress>', game.Event)
    root.bind('<KeyRelease>', game.Event)


def Update():
    canvas2d.Draw(canvas, game)
    canvas3d.Draw(canvas, game)
    game.MoveUnits()


def Loop():
    while True:
        Update()
        time.sleep(0.1)


canvas2d.Init(canvas, game)
canvas3d.Init(canvas, game)
threading.Timer(0, Loop).start()
root.mainloop()
