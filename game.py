import random

from units import Player, Wall
import math


class Game:
    def __init__(self, width, height):
        self.width = width
        self.height = height
        self.units = []
        self.player = None

    def AddUnit(self, unit):
        if isinstance(unit, Player):
            self.player = unit
        self.units.append(unit)
        print('self.units=', self.units)

    def Event(self, event):
        print('Game.Event(', event)
        for unit in self.units:
            unit.Event(event)

    def MoveUnits(self):
        for unit in self.units:
            unit.Move()

    def WallsAroundEdge(self):

        # top
        self.AddUnit(Wall(
            self.width / 2,
            1,
            height=10,
            width=self.width,
            game=self,
        ))

        # right
        self.AddUnit(Wall(
            self.width - 1,
            self.height / 2 + 1,
            height=self.height,
            width=10,
            game=self,
        ))

        # bottom
        self.AddUnit(Wall(
            self.width / 2 + 1,
            self.height - 2,
            height=5,
            width=self.width,
            game=self,
        ))

        # left
        self.AddUnit(Wall(
            1,
            self.height / 2,
            width=10,
            height=self.height,
            game=self,
        ))

    def RandomWalls(self):
        for i in range(random.randint(1, 10)):
            self.AddUnit(Wall(
                x=random.randint(0, self.width),
                y=random.randint(0, self.height),
                width=random.randint(1, 100),
                height=random.randint(1, 100),
                game=self,
            ))
