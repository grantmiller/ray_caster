import math

from units import Player, Wall, Ray, NPC

RAY_MAX_LEN = 0


def Init(canvas, game):
    global RAY_MAX_LEN
    # background color for the left side of screen
    canvas.create_line(game.width, 0, game.width, game.height, fill='black')  # divide the canvas in half
    RAY_MAX_LEN = math.hypot(game.width, game.height)
    print('RAY_MAX_LEN=', RAY_MAX_LEN)


canvasObjects = {
    # obj: int(canvasID),
}
rays = {
    # str(ID): rayObj
}


def GetRay(ID):
    return rays.get(ID, None)


def GetCanvasID(unit):
    return canvasObjects.get(unit, None)


def Draw(canvas, game):
    # draw the units
    for unit in game.units:
        if isinstance(unit, Player) or isinstance(unit, NPC):
            DrawPlayer(canvas, unit)
            if isinstance(unit, Player):
                DrawRays(
                    canvas,
                    unit,
                    wallUnits=[u for u in game.units if isinstance(u, Wall)],
                    npcUnits=[u for u in game.units if isinstance(u, NPC)],
                )

        elif isinstance(unit, Wall):
            DrawWall(canvas, unit)


def DrawWall(canvas, unit):
    obj = GetCanvasID(unit)
    if obj is None:
        obj = canvas.create_rectangle(*unit.coords, fill=unit.color)
        canvasObjects[unit] = obj

    canvas.coords(obj, unit.coords)


def DrawPlayer(canvas, unit):
    # draw the circle
    obj = GetCanvasID(unit)
    if obj is None:
        obj = canvas.create_oval(*unit.coords, fill=unit.color)
        canvasObjects[unit] = obj

    canvas.coords(obj, unit.coords)


def DrawRays(canvas, unit, wallUnits, npcUnits):
    # draw the rays
    for i in range(unit.viewSteps):
        rayID = f'{unit.id}_angle_{i}'
        ray = GetRay(rayID)
        if ray is None:
            ray = Ray(0, 0, 0, length=RAY_MAX_LEN)
            rays[rayID] = ray

        ray.x = unit.x
        ray.y = unit.y
        ray.angle = unit.angleViewStart + (unit.angleStep * i)

        rayLineID = GetCanvasID(ray)
        if rayLineID is None:
            rayLineID = canvas.create_line(0, 0, 0, 0)
            canvasObjects[ray] = rayLineID

        c = (
            *unit.middle,
            unit.middle[0] + math.cos(ray.angle) * RAY_MAX_LEN,
            unit.middle[1] + math.sin(ray.angle) * RAY_MAX_LEN,
        )
        canvas.coords(rayLineID, c)

        lineColor = 'yellow'
        for npc in npcUnits:
            if RayIsTouching(rayLineID, canvas, npc):
                lineColor = 'white'
                # canvas.itemconfig(rayLineID, fill='white')
                break
        else:
            for wallUnit in wallUnits:
                if RayIsTouching(rayLineID, canvas, wallUnit):
                    lineColor = 'purple'
                    # canvas.itemconfig(rayLineID, fill='purple')
                    break
            else:
                # canvas.itemconfig(rayLineID, fill='yellow')
                pass
        canvas.itemconfig(rayLineID, fill=lineColor)


def RayIsTouching(rayLineID, canvas, unit):
    # return the distance to the unit or False
    IDs = canvas.find_overlapping(*unit.coords)
    if rayLineID in IDs:
        ret = True
    else:
        ret = False
    return ret
