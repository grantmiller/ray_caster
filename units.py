import math
import random

from helpers import PointInsideRect

STEP_SIZE = 15
MIN_ANGLE = math.pi / 20


class Unit:
    def __init__(self, x, y, color, game, viewWindow=math.pi / 4, viewSteps=32, ):
        self.x = x  # middle of unit
        self.y = y  # middle of unit
        self.color = color
        self.game = game
        self.angle = 0
        self.size = 25
        self.id = str(id(self))
        self._keysDown = set()
        self.viewWindow = viewWindow
        self.viewSteps = int(viewSteps)

        self.angleStep = (self.angleViewEnd - self.angleViewStart) / self.viewSteps
        print('self.angleStep=', self.angleStep)
        print('self.angleViewStart=', self.angleViewStart)
        print('self.angleViewEnd=', self.angleViewEnd)
        print('self.viewSteps=', self.viewSteps)

    @property
    def angleViewStart(self):
        return self.angle - self.viewWindow / 2

    @property
    def angleViewEnd(self):
        return self.angle + self.viewWindow / 2

    def MoveStraight(self, steps):
        xDelta = math.cos(self.angle) * steps
        yDelta = math.sin(self.angle) * steps
        newXY = self.x + xDelta, self.y + yDelta
        self._NewPosition(newXY)

    def Turn(self, deltaAngle):
        self.angle += deltaAngle

    def Strafe(self, steps):
        xDelta = math.cos(self.angle - math.pi / 2) * steps
        yDelta = math.sin(self.angle - math.pi / 2) * steps
        newXY = self.x + xDelta, self.y + yDelta
        self._NewPosition(newXY)

    def _NewPosition(self, newXY):
        if not self._WallBlock(newXY):
            self.x, self.y = newXY
        self._LimitMovementWithinGame()

    def _LimitMovementWithinGame(self):
        if self.x < 0:
            self.x = 0
        elif self.x > self.game.width:
            self.x = self.game.width

        if self.y < 0:
            self.y = 0
        elif self.y > self.game.height:
            self.y = self.game.height

    def _WallBlock(self, newMiddle):
        for wall in [unit for unit in self.game.units if isinstance(unit, Wall)]:
            if PointInsideRect(*newMiddle, *wall.coords):
                return True
        return False

    @property
    def coords(self):
        return (
            self.x - self.size / 2,
            self.y - self.size / 2,
            self.x + self.size / 2,
            self.y + self.size / 2,
        )

    @property
    def middle(self):
        return (
            self.x,
            self.y,
        )

    def Event(self, event):
        # print('Unit.Event(', self, event)
        if str(event.type) == 'KeyPress':
            self._keysDown.add(event.keysym)
        elif str(event.type) == 'KeyRelease':
            if event.keysym in self._keysDown:
                self._keysDown.remove(event.keysym)

    def Move(self):
        raise NotImplementedError

    @property
    def topLeft(self):
        return self.x - self.width / 2, self.y - self.height / 2

    @property
    def topRight(self):
        return self.x + self.width / 2, self.y - self.height / 2

    @property
    def bottomLeft(self):
        return self.x - self.width / 2, self.y + self.height / 2

    @property
    def bottomRight(self):
        return self.x + self.width / 2, self.y + self.height / 2


class Player(Unit):
    def __init__(self, x, y, game):
        super().__init__(x, y, game=game, color='green')

    def Move(self):
        for key in ['w', 'a', 's', 'd', 'Left', 'Right']:
            if key in self._keysDown:
                {
                    'w': lambda: self.MoveStraight(STEP_SIZE),
                    'a': lambda: self.Turn(0 - MIN_ANGLE),
                    's': lambda: self.MoveStraight(0 - STEP_SIZE),
                    'd': lambda: self.Turn(MIN_ANGLE),
                    'Left': lambda: self.Strafe(STEP_SIZE),
                    'Right': lambda: self.Strafe(0 - STEP_SIZE),
                }.get(key, lambda: None)()


class NPC(Unit):
    def __init__(self, *a, **k):
        super().__init__(*a, color='red', **k)
        self.width = 25
        self.height = 25

    def Move(self):
        random.choice([
            lambda: self.MoveStraight(STEP_SIZE),
            lambda: self.Turn(0 - MIN_ANGLE),
            lambda: self.MoveStraight(0 - STEP_SIZE),
            lambda: self.Turn(MIN_ANGLE),
            lambda: self.Strafe(STEP_SIZE),
            lambda: self.Strafe(0 - STEP_SIZE),
        ])()


class Wall(Unit):
    def __init__(self, x, y, width, height, game):
        super().__init__(x, y, color='blue', game=game)
        # x, y = middle
        self.width = width
        self.height = height
        self.size = 100  # always a square

    @property
    def coords(self):
        return (
            self.x - self.width / 2,
            self.y - self.height / 2,
            self.x + self.width / 2,
            self.y + self.height / 2,
        )

    def Move(self):
        pass

    def __str__(self):
        return f'<Wall: x={self.x}, y={self.y}, width={self.width}, height={self.height}>'


class Ray:
    def __init__(self, x, y, angle, length):
        self.x = x  # staring point
        self.y = y  # staring point
        self.angle = angle
        self.length = length

    @property
    def start(self):
        return self.x, self.y

    @property
    def end(self):
        return (
            self.start[0] + (math.cos(self.angle) * self.length),
            self.start[1] + (math.sin(self.angle) * self.length),
        )
