import math

import canvas2d
from helpers import PointInsideRect
from units import Wall, NPC

OFFSET_X = 0
WALL_COLOR = '#999999'

def Init(canvas, game):
    global OFFSET_X
    OFFSET_X = game.width


rects = {
    # obj: canvasID,
}


def GetRectID(obj):
    return rects.get(obj, None)


def Draw(canvas, game):
    distance = None

    for index, tup in enumerate(canvas2d.rays.items()):
        ID, ray = tup
        rayLineID = canvas2d.GetCanvasID(ray)

        isTouchingSomething = False
        shortestDistance = None
        barColor = WALL_COLOR
        for unit in [u for u in game.units if isinstance(u, Wall) or isinstance(u, NPC)]:

            if canvas2d.RayIsTouching(rayLineID, canvas, unit):
                isTouchingSomething = True

                lineTop = (unit.topLeft, unit.topRight)
                lineBottom = (unit.bottomLeft, unit.bottomRight)
                lineRight = (unit.topRight, unit.bottomRight)
                lineLeft = (unit.topLeft, unit.bottomLeft)

                lineRay = (ray.start, ray.end)

                for line in [lineTop, lineBottom, lineRight, lineLeft]:
                    intersectionPoint = line_intersection(lineRay, line)
                    if intersectionPoint and PointInsideRect(*intersectionPoint, *unit.coords):

                        distanceToIntersection = math.hypot(
                            ray.start[0] - intersectionPoint[0],
                            ray.start[1] - intersectionPoint[1],
                        )
                        if shortestDistance is None or distanceToIntersection < shortestDistance:
                            shortestDistance = distanceToIntersection

                            if isinstance(unit, NPC):
                                barColor = '#ff0000' # red
                            else:
                                barColor = WALL_COLOR

        distance = shortestDistance

        rectID = GetRectID(ray)
        if rectID is None:
            rectID = canvas.create_rectangle(0, 0, 0, 0, fill=WALL_COLOR, outline=WALL_COLOR)
            rects[ray] = rectID

        if isTouchingSomething:
            height = game.height - distance
            color = Darken(barColor, darkLevel=int(distance / 4))
            width = game.width / len(canvas2d.rays)
            x = OFFSET_X + (index / len(canvas2d.rays)) * game.width
            y = (game.height - height) / 2
            c = (
                x,
                y,
                x + width,
                y + height,
            )
            canvas.coords(rectID, c)
            canvas.itemconfig(rectID, fill=color, outline=color)
        else:
            canvas.delete(rectID)
            rects.pop(ray, None)


def line_intersection(line1, line2):
    # kudos: https://stackoverflow.com/questions/20677795/how-do-i-compute-the-intersection-point-of-two-lines
    # line1 in for form ((x1, y1), (x2, y2))
    xdiff = (line1[0][0] - line1[1][0], line2[0][0] - line2[1][0])
    ydiff = (line1[0][1] - line1[1][1], line2[0][1] - line2[1][1])

    def det(a, b):
        return a[0] * b[1] - a[1] * b[0]

    div = det(xdiff, ydiff)
    if div == 0:
        return None

    d = (det(*line1), det(*line2))
    x = det(d, xdiff) / div
    y = det(d, ydiff) / div

    return x, y


def Darken(colorHex, darkLevel):
    # darkLevel > int ; higher the darker
    if colorHex.startswith('#'):
        colorHex = colorHex[1:]

    parts = [
        bytes.fromhex(colorHex[:2]),
        bytes.fromhex(colorHex[2:4]),
        bytes.fromhex(colorHex[4:]),
    ]

    for i in range(darkLevel):
        for index, part in enumerate(parts.copy()):
            i = int.from_bytes(part, byteorder='big')
            i -= 1
            if i < 0:
                i = 0
            b = i.to_bytes(length=1, byteorder='big')
            parts[index] = b

    return f'#{parts[0].hex()}{parts[1].hex()}{parts[2].hex()}'


